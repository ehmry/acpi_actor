# Package

version = "20240208"
author        = "Emery Hemingway"
description   = "Syndicate actor for publishing Linux ACPI events"
license       = "Unlicense"
srcDir        = "src"
bin           = @["acpi_actor"]


# Dependencies

requires "nim >= 1.6.12", "syndicate >= 20230518"
