# acpi_actor
A Syndicate actor for publishing Linux ACPI events as well as an example of interacting with Linux generic netlink sockets in pure Nim (Linux C headers aside).

A configuration example is provide at [example-config.pr](./example-config.pr).

Not very useful. [Syndev](https://git.syndicate-lang.org/ehmry/syndev) is probably more informative.
